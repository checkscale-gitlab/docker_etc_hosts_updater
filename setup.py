from setuptools import setup, find_packages

setup(
    name='docker-etc-hosts-updater',
    version='0.0.3',
    install_requires=[
        'docker',
        'python-daemon',
        'lockfile'
    ],
    packages=find_packages(),
    scripts=["bin/docker_etc_hosts_updater"],
    url='',
    license='',
    author='Christian Staude',
    author_email='christian@unfiysell.de',
    description='Some desc',
    keywords="docker hosts /etc/hosts updater",

)
